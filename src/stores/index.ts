import { createPinia } from 'pinia'

const store = createPinia()

export * from './modules/app'
export * from './modules/user'
export * from './modules/main-state'

export default store
