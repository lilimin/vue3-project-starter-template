import { describe, it, expect } from 'vitest'

import { mount } from '@vue/test-utils'
import XPagination from '../index.vue'

describe('HelloWorld', () => {
	it('test total and index', () => {
		const wrapper = mount(XPagination, { props: { index: 4, limit: 10, state: { total: 100 } } })
		expect(wrapper.text()).toContain('100')
		expect(wrapper.text()).toContain('4')
	})
})
