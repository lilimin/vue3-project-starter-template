import type { App } from 'vue'

import Pagination from './x-pagination/index.vue'
import TableHead from './x-table-head/index.vue'
import TableEditField from './x-table-edit-field/index.vue'
import FormDialog from './x-form-dialog/index.vue'
import Icon from './x-icon/index.vue'
import UploadImage from './x-upload-image/index.vue'
import UploadFile from './x-upload-file/index.vue'
import RichEditor from './x-rich-editor/index.vue'

const cps = {
	XPagination: Pagination,
	XTableHead: TableHead,
	XTableEditField: TableEditField,
	XFormDialog: FormDialog,
	XIcon: Icon,
	XUploadImage: UploadImage,
	XUploadFile: UploadFile,
	XRichEditor: RichEditor,
}

export default {
	...cps,
	install: (app: App) => {
		Object.keys(cps).forEach((key) => {
			app.component(key, cps[key])
		})
	},
}

/** === 使用unplugin-vue-components后可自动导入组件 */
// declare module '@vue/runtime-core' {
// 	type Components = typeof cps
// 	// 声明全局组件，继承所有components的属性
// 	export interface GlobalComponents extends Components {}
// 	interface ComponentCustomProperties {
// 		//   $message: typeof import('element-plus')['ElMessage']
// 	}
// }
// export {} // 如果单独定义 x.d.ts 需要保留本行
