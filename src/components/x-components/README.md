## X-Components

基于 element-plus 拓展的一些常用业务组件，每个组件用法参见组件注释

## 全局安装组件

```
# src/main.ts

// ...
const app = createApp(App)

import XComponents from '@/components/x-components'
app.use(XComponents)

```

在 vue 组件中使用

```

# xxx.vue

<template>
	<x-pagination :index="1" :limit="10" :state="{total:0}" />
</template>

```

> 不需要 import，不需要额外的组件注册，同时 volar 也会正确的识别这个组件，并知道该组件的 props 等属性

## 实现全局注册和 typescript 声明的方法

主要参考 /src/components/x-components/index.ts

其中引入了所有组件，并提供了 install 方法给 vue 进行安装。

同时，通过如下方法巧妙的向 typescript 声明了所有组件的内容

```
declare module '@vue/runtime-core' {
	type Components = typeof components
	export interface GlobalComponents extends Components { }
}
```

参考 element-plus 的方式，其实应该提供一个 global.d.ts 来单独声明类型

```
# tsconfig.json
"compilerOptions":{
	"types":[..., "element-plus/global"]
}
```
