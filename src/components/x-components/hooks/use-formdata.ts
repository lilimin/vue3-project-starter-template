import { reactive, type UnwrapNestedRefs } from 'vue'
import { cloneDeep } from 'lodash-es'

/**
 * 创建可重置的表单数据对象
 * @param init 表单数据对象
 */
const useFormdata = <T extends object>(init: T) => {
	const initData = cloneDeep(init)
	const formdata = reactive(cloneDeep(init))
	return {
		formdata,
		reset: () => Object.assign(formdata, initData),
	}
}

export default useFormdata
