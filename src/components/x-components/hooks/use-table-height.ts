import { ref } from 'vue'

/**
 * 自动计算通用table的高度
 */

const table_height = ref('calc(100vh - 238px)')

const useTableHeight = () => {
	return table_height
}

export default useTableHeight
