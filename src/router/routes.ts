import type { RouteRecordRaw, RouteLocationNormalized, NavigationGuardNext } from 'vue-router'
import { useUserStore } from '@/stores/modules/user'
import ViewMain from '@/common/views/main.vue'

const routes: RouteRecordRaw[] = [
	{
		path: '/login',
		name: 'login',
		meta: {
			title: '登陆',
			login: false,
		},
		component: () => import('@/common/views/login.vue'),
		beforeEnter: (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
			// 如果已有登录态，不允许进入登录页
			const user_store = useUserStore()
			if (user_store.info) return next(from)
			else {
				return user_store
					.updateInfo(true)
					.then(() => next(from))
					.catch(() => next())
			}
		},
	},
	{
		path: '/control_center',
		alias: '/',
		redirect: { name: 'control_dashboard' },
		meta: { title: '控制中心' },
		component: ViewMain,
		children: [
			{
				path: 'dashboard',
				name: 'control_dashboard',
				meta: {
					title: '控制面板',
				},
				component: () => import('@/views/control/dashboard.vue'),
			},
		],
	},
	{
		path: '/table',
		redirect: { name: 'table_base_example' },
		component: ViewMain,
		children: [
			{
				path: 'base_example',
				name: 'table_base_example',
				meta: {
					title: '表格基础示例',
					breadcrumb: [{ name: '表格' }, { name: '基础示例' }],
				},
				component: () => import('@/views/table/base-example.vue'),
			},
			{
				path: 'column_filter_example',
				name: 'table_column_filter_example',
				meta: {
					title: '表格列显隐',
					breadcrumb: [{ name: '表格' }, { name: '列显隐' }],
				},
				component: () => import('@/views/table/table-column-filter.vue'),
			},
			{
				path: 'column_filter_example_new',
				name: 'table_column_filter_example_new',
				meta: {
					title: '表格列显隐（新）',
					breadcrumb: [{ name: '表格' }, { name: '列显隐（新）' }],
				},
				component: () => import('@/views/table/table-column-filter-new.vue'),
			},
		],
	},
	{
		path: '/form',
		redirect: { name: 'form_base_example' },
		component: ViewMain,
		children: [
			{
				path: 'base_example',
				name: 'form_base_example',
				meta: {
					title: '表单',
					breadcrumb: [{ name: '表单' }, { name: '基础示例' }],
				},
				component: () => import('@/views/form/base-example.vue'),
			},
		],
	},

	/** 通配异常页面 */
	{
		path: '/:pathMatch(.*)*',
		name: 'error',
		meta: {
			login: false,
		},
		component: () => import('@/common/views/error.vue'),
		props: {
			icon: 'warning',
			title: '404 页面找不到啦~',
			description: '生活总归带点荒谬',
		},
	},
]

export default routes
