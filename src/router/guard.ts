import type { RouteLocationNormalized, NavigationGuardNext } from 'vue-router'

/**
 * 全局前置守卫
 */
export const beforeEach = (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
	// 验证登录状态
	const isUserLogined = true
	const checkLogin = () => new Promise((resolve, reject) => reject('用户未登录'))
	if (to.meta?.login !== false && !isUserLogined) {
		return checkLogin()
			.then(() => next())
			.catch((e) => next({ name: 'login' }))
	}

	next()
}

/**
 * 全局后守卫
 */
export const afterEach = async (to: RouteLocationNormalized, from: RouteLocationNormalized) => {
	// 页面标题
	if (typeof to.meta.title == 'string') document.title = to.meta.title
}
