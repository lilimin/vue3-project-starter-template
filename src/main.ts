/**
 * === 样式 ===
 * 注意css的引入顺序会互相影响
 * 特别是一些UI组件库的css
 */
// 加载tailwind.css
import './tailwind.css'
// 加载element-plus一些特殊组件样式
import 'element-plus/es/components/message/style/css'
import 'element-plus/es/components/message-box/style/css'
import 'element-plus/es/components/notification/style/css'
// 加载element-plus黑夜模式
import 'element-plus/theme-chalk/dark/css-vars.css'
// 加载应用主题
import './theme.less'

/** === App入口文件 */
import { createApp } from 'vue'
import App from './App.vue'

// 实例化Vue App
const app = createApp(App)

/** === pinia === */
import { createPinia } from 'pinia'
app.use(createPinia())

/** === vue-router === */
import router from './router'
app.use(router)

/** === element-plus全局组件 === */
import { ElMessage, ElMessageBox, ElNotification } from 'element-plus'
app.use(ElMessage).use(ElMessageBox).use(ElNotification)

/** === 应用组件 x-components === */
import XComponents from '@/components/x-components'
app.use(XComponents)

/** === Dayjs === */
import dayjs from 'dayjs'
import dayjsLocaleZhCn from 'dayjs/locale/zh-cn.js'
dayjs.locale(dayjsLocaleZhCn)
app.config.globalProperties.$dayjs = dayjs

/** === Vue App 挂载 */
app.mount('#app')
