import request from '@/libs/request'
import type { ResponsePageableData } from '@/libs/request'

export const uploadFile = (file, onUploadProgress?) => {
	const fd = new FormData()
	fd.append('file', file)
	return request({ url: 'commonapi/Upload/uploadFile', data: fd, onUploadProgress })
}

export const uploadImage = (file, onUploadProgress?) => {
	const fd = new FormData()
	fd.append('file', file)
	return request({ url: 'commonapi/Upload/uploadImage', data: fd, onUploadProgress })
}

/** 设置模型字段 */
export const setModelField = (data: {
	/** 模型名 */
	model: string
	/** 主键ID */
	id: number | string
	/** 字段名 */
	field: string
	/** 字段值 */
	value: any
}) => request({ url: 'commonapi/Other/setModelField', data })
