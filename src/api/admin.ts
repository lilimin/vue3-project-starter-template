import request from '@/libs/request'
import type { ResponsePageableData, ResponseCommonData } from '@/libs/request'
import { getToken } from '@/libs/local-store'
import type { UserInfo } from '@/stores/modules/user'

// [Demo] 登陆
export const login = (data: { account: string; pswd: string }) => request({ url: 'adminapi/Admin/login', data })

// [Demo] 获取管理员信息
export const getAdminInfo = (silence = false) =>
	request({ url: 'adminapi/Admin/getDetail', auto_message_when_error: !silence })
