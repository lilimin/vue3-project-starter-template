/**
 * 复制文本
 * @param text 复制的内容
 */
export const copyText = (text: string | number) => {
	let inputDom = document.createElement('textarea')
	// @ts-ignore
	inputDom.style =
		'opacity:0;position:fixed;top:0;left:-99999;width:1px;height:1px;overflow:hidden;padding:0;margin:0;'
	document.body.appendChild(inputDom)

	inputDom.value = text.toString()
	inputDom.select()
	document.execCommand('copy')

	document.body.removeChild(inputDom)
}
