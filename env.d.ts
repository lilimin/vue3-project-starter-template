/// <reference types="vite/client" />
// 定义env中的变量
interface ImportMetaEnv {
	/** API基础路径 */
	VITE_API_BASE_URL: string
	/** local-storage前缀 */
	VITE_STORAGE_PREFIX: string
	/** iconfont.js图标的路径，可以是cdn或者放在public中，写成./iconfont.js相对路径 */
	VITE_ICONFONT_URL: string
	/** 高德地图key */
	VITE_AMAP_KEY: string
}

// 定义window上挂载的变量
interface Window {
	CONFIG: any
}
