// generated by unplugin-vue-components
// We suggest you to commit this file into source control
// Read more: https://github.com/vuejs/core/pull/3399
import '@vue/runtime-core'

export {}

declare module '@vue/runtime-core' {
	export interface GlobalComponents {
		ElAside: typeof import('element-plus/es')['ElAside']
		ElAvatar: typeof import('element-plus/es')['ElAvatar']
		ElBreadcrumb: typeof import('element-plus/es')['ElBreadcrumb']
		ElBreadcrumbItem: typeof import('element-plus/es')['ElBreadcrumbItem']
		ElButton: typeof import('element-plus/es')['ElButton']
		ElButtonGroup: typeof import('element-plus/es')['ElButtonGroup']
		ElCard: typeof import('element-plus/es')['ElCard']
		ElCheckbox: typeof import('element-plus/es')['ElCheckbox']
		ElCol: typeof import('element-plus/es')['ElCol']
		ElConfigProvider: typeof import('element-plus/es')['ElConfigProvider']
		ElContainer: typeof import('element-plus/es')['ElContainer']
		ElDialog: typeof import('element-plus/es')['ElDialog']
		ElDivider: typeof import('element-plus/es')['ElDivider']
		ElDropdown: typeof import('element-plus/es')['ElDropdown']
		ElDropdownItem: typeof import('element-plus/es')['ElDropdownItem']
		ElDropdownMenu: typeof import('element-plus/es')['ElDropdownMenu']
		ElFooter: typeof import('element-plus/es')['ElFooter']
		ElForm: typeof import('element-plus/es')['ElForm']
		ElFormItem: typeof import('element-plus/es')['ElFormItem']
		ElHeader: typeof import('element-plus/es')['ElHeader']
		ElIcon: typeof import('element-plus/es')['ElIcon']
		ElImage: typeof import('element-plus/es')['ElImage']
		ElInput: typeof import('element-plus/es')['ElInput']
		ElInputNumber: typeof import('element-plus/es')['ElInputNumber']
		ElMain: typeof import('element-plus/es')['ElMain']
		ElMenu: typeof import('element-plus/es')['ElMenu']
		ElMenuItem: typeof import('element-plus/es')['ElMenuItem']
		ElPagination: typeof import('element-plus/es')['ElPagination']
		ElProgress: typeof import('element-plus/es')['ElProgress']
		ElRow: typeof import('element-plus/es')['ElRow']
		ElSubMenu: typeof import('element-plus/es')['ElSubMenu']
		ElSwitch: typeof import('element-plus/es')['ElSwitch']
		ElTable: typeof import('element-plus/es')['ElTable']
		ElTableColumn: typeof import('element-plus/es')['ElTableColumn']
		ElTag: typeof import('element-plus/es')['ElTag']
		ElTooltip: typeof import('element-plus/es')['ElTooltip']
		ElUpload: typeof import('element-plus/es')['ElUpload']
		RouterLink: typeof import('vue-router')['RouterLink']
		RouterView: typeof import('vue-router')['RouterView']
		XFormDialog: typeof import('./src/components/x-components/x-form-dialog/index.vue')['default']
		XIcon: typeof import('./src/components/x-components/x-icon/index.vue')['default']
		XPagination: typeof import('./src/components/x-components/x-pagination/index.vue')['default']
		XRichEditor: typeof import('./src/components/x-components/x-rich-editor/index.vue')['default']
		XTableEditField: typeof import('./src/components/x-components/x-table-edit-field/index.vue')['default']
		XTableHead: typeof import('./src/components/x-components/x-table-head/index.vue')['default']
		XUploadFile: typeof import('./src/components/x-components/x-upload-file/index.vue')['default']
		XUploadImage: typeof import('./src/components/x-components/x-upload-image/index.vue')['default']
	}
	export interface ComponentCustomProperties {
		vLoading: typeof import('element-plus/es')['ElLoadingDirective']
	}
}
