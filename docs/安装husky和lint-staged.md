可以使用

```
npx mrm lint-staged
```

一键安装和配置 husky 和 lint-staged

如果遇到报错，可能是 mrm 版本过高，可以降低版本

```
npx mrm@2 lint-staged
```
